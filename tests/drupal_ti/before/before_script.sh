#!/bin/bash

# Add an optional statement to see that this is running in Travis CI.
echo "running drupal_ti/before/before_script.sh"

#!/bin/bash

### Overwrite default behat before_script
# Simple script to install drupal for travis-ci running.

set -e $DRUPAL_TI_DEBUG

# Ensure the right Drupal version is installed.
drupal_ti_ensure_drupal

# Ensure the module is linked into the code base and enabled.
#drupal_ti_ensure_module

# Clear caches and run a web server.
drupal_ti_clear_caches
drupal_ti_run_server

# Start xvfb and selenium.
drupal_ti_ensure_xvfb
drupal_ti_ensure_webdriver



# Drush configuration
drush site-alias @self --full --with-optional >> ~/.drush/mysite.aliases.drushrc.php
drush dl drupalorg_drush
drush dl registry_rebuild
drush cc drush
echo "available drush site aliases"
drush sa



### Drupal installation

cd "$DRUPAL_TI_DRUPAL_DIR/profiles"
git clone --branch 7.x-1.x https://github.com/proudcity/proud.git
cd proud
#drush verify-makefile drupal-org.make
drush make --no-cache --drupal-org drupal-org.make .
drush make libraries.make . --no-core --contrib-destination=.

cd "$DRUPAL_TI_DRUPAL_DIR/sites/all/modules"
mkdir apps
mkdir dev
mkdir contrib
mkdir custom
cd apps
git clone --branch 7.x-1.x https://github.com/proudcity/proud_faq.git
git clone --branch 7.x-1.x https://github.com/proudcity/proud_event.git
git clone --branch 7.x-1.x https://github.com/proudcity/proud_landingpage.git
git clone --branch 7.x-1.x https://github.com/proudcity/proud_news.git
git clone --branch 7.x-1.x https://github.com/proudcity/proud_job.git
git clone https://github.com/proudcity/proud_webform.git
git clone https://github.com/proudcity/proud_people.git
git clone https://github.com/proudcity/proud_agency.git
git clone https://github.com/proudcity/proud_document.git
git clone --branch 7.x-1.x https://github.com/proudcity/proud_social.git
#git clone --branch 7.x-1.x https://github.com/proudcity/proud_311_app.git
#git clone --branch 7.x-1.x https://github.com/proudcity/proud_map_app.git
git clone --branch 7.x-1.x https://github.com/proudcity/proud_search.git
git clone --branch 7.x-1.x https://github.com/proudcity/proudcity_pages.git

cd ../dev
git clone --branch 7.x-1.x http://git.drupal.org/project/angular_media.git
git clone --branch 7.x-1.x http://git.drupal.org/project/media_formatters.git
git clone --branch 7.x-1.x http://git.drupal.org/project/ckeditor_widgets.git
git clone --branch 7.x-1.x http://git.drupal.org/project/ckeditor_filter.git
git clone --branch 7.x-1.x http://git.drupal.org/sandbox/jlyon/socialfield_widgets.git social_field_widgets
git clone --branch 7.x-1.x http://git.drupal.org/sandbox/jlyon/2564675.git google_analytics_embed
git clone --branch 7.x-1.x http://git.drupal.org/sandbox/jlyon/2564689.git bootstrap_fieldable_panel_panes
git clone --branch 7.x-1.x http://git.drupal.org/project/proud_panels.git
git clone --branch 7.x-1.x http://git.drupal.org/project/helpful_formatters.git
git clone --branch 7.x-1.x http://git.drupal.org/sandbox/jlyon/2591751.git addthisevent_formatter

cd ../custom
#git@github.com:proudcity/proudcity_demo.git

mkdir ../../themes/dev
cd ../../themes/dev
git clone --branch 7.x-1.x http://git.drupal.org/project/minimalist_admin.git
git clone --branch 7.x-1.x http://git.drupal.org/project/proudtheme.git

drush dl date_ical geophp pdf_reader search_api_db workbench_moderation better_exposed_filters facetapi geocoder hotjar registration services cors geocoder_autocomplete office_hours restws_search_api socialfield webform date field_group geofield search_api workbench

cd $DRUPAL_TI_DRUPAL_DIR
wget http://dev.getproudcity.com/proudcity.sql.gz
gunzip $DRUPAL_TI_DRUPAL_DIR/proudcity.sql.gz
drush sqlc < proudcity.sql
drush rr


cd $TRAVIS_BUILD_DIR
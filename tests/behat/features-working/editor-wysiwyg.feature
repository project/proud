Feature: Use administration functions
  In order to maintain the website
    As an editor
    I need to be able to create and edit content



@api
Scenario: Create nodes with specific authorship
  And "article" content:
  | title          | body             | promote |
  | Article by Joe | PLACEHOLDER BODY | 1       |
  When I am logged in as a user with the "administrator" role
  And I am on the homepage
  And I follow "Article by Joe"
  Then I should see the link "Joe User"
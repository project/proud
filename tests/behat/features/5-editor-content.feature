Feature: Use administration functions
  In order to maintain the website
    As an editor
    I need to be able to create and edit content


# Require a real browser. Will use Selenium/Firefox (or Zombie or Sahi).
@api
@javascript
Scenario: 5b: Editor can use angular_media in CKEditor
 When I am logged in as a user with the "administrator" role
    And I am on "/node/add/page"
    And I click the "image" button in the "edit-body-und-0-value" WYSIWYG editor
    And I wait for 1 seconds
    And I press "Upload/Browse Server"
  Then I should see "Drop files here"

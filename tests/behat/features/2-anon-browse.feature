Feature: Browse site
  In order to use my city website
    As a site visitor
    I need to be able to find what I am looking for.

Scenario: 2a: View calendar of upcoming events.
  Given I am on the homepage
  When I click "Events"
  Then I should see "Upcoming events"
  And I should see "Add to Calendar"

Scenario: 2b: View list of upcoming events.
  Given I am on the homepage
  When I click "News"
  Then I should see "Recent news"
  And I should see "Filter"

@javascript
Scenario: 2c: Find contact information for police department.
  Given I am on the homepage
  When I click "Government"
  And I enter "police" for "Quickfind"
  And I wait for 1 seconds
  And I click "Police Department"
  Then I should see "Hours"
  And I should see "Directions"

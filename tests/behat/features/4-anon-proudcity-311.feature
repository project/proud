Feature: ProudCity 311 Features
  In order to effectively use my city website
    As a site visitor
    I need to be able to pay fines, report issues and find answers.

@javascript
Scenario: 3b: Get directed to pay a water bill from search.  @todo: work with Stripe iFrame
  Given I am on the homepage
  And I wait for 10 seconds
  And I click "Make a Payment"
  And I wait for 1 seconds
  And I click "Parking ticket"
  And I enter "0001" for "invoice_number"
  And I wait for 1 seconds
  And I should see "101.01"
  And I press "Make payment"
  And I wait for 1 seconds
  And I enter "test@geproudcity.com" for "email"
  And I enter "4242 4242 4242 4242" for "Card number"
  And I enter "01/22" for "cc-exp"
  And I enter "123" for "cc-csc"
  And I click play
  Then I should see "Thank you"
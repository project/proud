Feature: Search site
  In order to use my city website
    As a site visitor
    I need to get useful search results.

Scenario: 3a: When I start a search, see suggested content.
  Given I am on the homepage
  And I enter "brew" for "proud-search-input"
  Then I should see "Brews and Views"

@javascript
Scenario: 3b: Get directed to pay a water bill from search.
  Given I am on the homepage
  And I enter "water" for "proud-search-input"
  And I click "Water bill"
  And I wait for 1 seconds
  Then I should see "Pay online now"
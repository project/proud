Feature: Browse site
  In order to use my city website
    As a site visitor
    I need to be able to find what I am looking for.

Scenario: 1a Site should have the latest Drupal security updates.
  Given I run drush "pm-updatestatus"
  Then drush output should not contain "SECURITY UPDATE available"
  

### Setting up
```
cd ./tests/behat
composer install
mkdir bin
cd bin
# Download selenium-server-standalone-*.jar from https://selenium-release.storage.googleapis.com/index.html
wget https://selenium-release.storage.googleapis.com/2.47/selenium-server-standalone-2.47.1.jar
```
# Update `./behat.yml` with base_url, wd_host setting that you got after starting selenium

### Running tests
```
cd ./tests/behat
java -jar bin/selenium-server-*.jar
# Update `./behat.yml` with base_url, wd_host values (after running command above)

# Run all tests
./bin/behat/

# Run specific test feature
./bin/behat features/4-anon-proudcity-311.feature
```

Helpful testing resources:
* http://kevinquillen.com/bdd/2014/06/08/your-first-behat-test/

<?php
/**
 * @file
 * proud_admin.pages.inc
 */
module_load_include('inc', 'proud_toolbar', 'includes/sidebar');


/**
 * Menu callback: The checklist at /admin/proud
 */
function proud_admin_checklist_form() {
  $form = array();

  $sidebar = proud_toolbar_view_sidebar();
  $links = $sidebar['sub_level'];
  unset($links['admin/config/proud/checklist']);

  $options = array();
  $status = variable_get('proud_admin_status', array());
  $next = NULL;
  $complete = TRUE;
  $i = 0;
  $step = 1;
  foreach ($links as $path => $item) {
    $key = strtolower($item['title']);
    $options[$key] = '<h4>' . l($item['title'], $path) . '</h4><p>' . $item['description'] . '</p>';
    if (!empty($status[$key]) && $status[$key]) {
      $i++;
      $step += $complete ? 1 : 0;
    }
    elseif($complete == TRUE) {
      $complete = FALSE;
      $next = l(t('Step !num: !title', array('!num' => $step, '!title' => $item['title'])), $path);
    }
  }
  $total = count($links);
  $percent = round($i/$total*100);

  //@todo: make this in _theme, create panel for this, nice msg when 100% done
  $form['progress'] = array(
    '#type' => 'markup',
    '#markup' => '
<style>
form .form-checkbox {
  position:absolute !important;
  margin-top: 25px !important;
}
form h4,form p {
  margin-left: 40px;
}
form h4 {
  margin-bottom: 0;
}
</style>
<div class="card">
<div class="card-block">
  <a class="btn btn-primary btn-xs pull-right">Launch setup wizard »</a>
  <h4 class="card-title">Setup progress <small> | <a href="#">'.$next.' &raquo;</a></small></h4>
  <div class="progress" style="margin-bottom:0;">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="'. $percent .'" aria-valuemin="0" aria-valuemax="100" style="width:'. $percent .'%">
      <span class="sr-only">40% Complete (success)</span>
    </div>
  </div>
</div>
</div>',
  );

  $form['proud_admin_status'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('proud_admin_status', array()),
  );

  $form = system_settings_form($form);
  $form['actions']['submit']['#value'] = t('Update progress');
  return $form;
}



/**
 * Menu callback: The checklist at /admin/proud
 */
function proud_admin_proudcity_form() {
  $form = array();

  $form['proudcity_city'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('City'),
    '#default_value' => variable_get('proudcity_city', ''),
  );
  $form['proudcity_state'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('State (full)'),
    '#default_value' => variable_get('proudcity_state', ''),
  );
  $form['proudcity_state'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('State (abbreviation)'),
    '#default_value' => variable_get('proudcity_state_abbr', ''),
  );
  $form['proudcity_lat'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('City latitude'),
    '#default_value' => variable_get('proudcity_lat', ''),
  );
  $form['proudcity_lng'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('City longitude'),
    '#default_value' => variable_get('proudcity_lng', ''),
  );
  $form['proudcity_lng'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('City longitude'),
    '#default_value' => variable_get('proudcity_lng', ''),
  );

  $form['googleanalytics_account'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Google Analytics web property id'),
    '#default_value' => variable_get('googleanalytics_account', 'UA-'),
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
 
  $hotjar = variable_get('hotjar_settings', array('hotjar_account' => ''));
  $form['advanced']['hotjar_settings[hotjar_account]'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Hotjar id'),
    '#default_value' => $hotjar['hotjar_account'],
    '#description' => t('Your Hotjar ID can be found in your tracking code on the line <code>h._hjSettings={hjid:<b>12345</b>,hjsv:5};</code> where <code><b>12345</b></code> is your Hotjar ID'),
  );

  $form['advanced']['proud_social_key'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Proud social key'),
    '#default_value' => variable_get('proud_social_key', ''),
  );
  $form['advanced']['proud_social_secret'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Proud social secret'),
    '#default_value' => variable_get('proud_social_secret', ''),
  );

  $form['advanced']['google_analytics_embed_view_id'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Google Analytics view id'),
    '#default_value' => variable_get('google_analytics_embed_view_id', ''),
    '#description' => t('This is different than your web property id. To generate this, click on Admin, select your Account and Property and click View Settings under the View tab.'),
  );
  $form['advanced']['google_analytics_embed_credential_file'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Google Analytics service account JSON key file'),
    '#default_value' => variable_get('google_analytics_embed_credential_file', ''),
    '#description' => 
      t('Enter the path to your Google Analytics Service Account JSON key file.  Read the !docs for help generating this. You must add this service account to the web property.',
      array('!docs' => l('Google Documentation', 'https://developers.google.com/identity/protocols/OAuth2ServiceAccount#creatinganaccount', array('target' => '_blank')))
    ),
  );

  return system_settings_form($form);
}